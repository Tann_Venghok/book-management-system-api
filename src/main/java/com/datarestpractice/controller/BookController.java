//package com.datarestpractice.controller;
//
//import com.datarestpractice.model.Book;
//import com.datarestpractice.restrepository.BookRestRepository;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.util.StringUtils;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.multipart.MultipartFile;
//import org.springframework.web.servlet.view.RedirectView;
//
//import java.io.IOException;
//
//@Controller
//public class BookController {
//    @Autowired
//    private BookRestRepository bookRestRepository;
//
//    @PostMapping("/save-photo")
//    public RedirectView saveUser(Book book,
//                                 @RequestParam("image") MultipartFile multipartFile) throws IOException {
//
//        String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
//        book.setThumbnail(fileName);
//
//        Book savedBook = bookRestRepository.save(book);
//
//        String uploadDir = "photo/" + savedBook.getId();
//
//        FileUploadUtil.saveFile(uploadDir, fileName, multipartFile);
//
//        return new RedirectView("/books", true);
//    }
//}

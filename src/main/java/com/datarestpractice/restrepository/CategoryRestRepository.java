package com.datarestpractice.restrepository;

import com.datarestpractice.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRestRepository extends JpaRepository<Category, Integer> {
}

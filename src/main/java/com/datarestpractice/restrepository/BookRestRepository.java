package com.datarestpractice.restrepository;

import com.datarestpractice.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRestRepository extends JpaRepository<Book, Integer>, PagingAndSortingRepository<Book, Integer> {
//    @RestResource(path = "des", rel = "searchByArticleDescription")
    @Query(value = "SELECT * FROM book WHERE category_id = :id", nativeQuery = true)
    List<Book> findByCategory(@Param("id") int id);
}

//package com.datarestpractice.validator;
//
//import com.datarestpractice.model.Person;
//import org.springframework.stereotype.Component;
//import org.springframework.validation.Errors;
//import org.springframework.validation.Validator;
//
////@Component("beforeCreatePersonValidator")
//public class PersonValidator implements Validator {
//    @Override
//    public boolean supports(Class<?> clazz) {
//        return Person.class.equals(clazz);
//    }
//
//    @Override
//    public void validate(Object target, Errors errors) {
//        Person p = (Person) target;
//        if (checkInputString(p.getFirstName())) {
//            errors.rejectValue("firstName", "name.empty", "field can't be null");
//        }
//        if (checkInputString(p.getLastName())) {
//            errors.rejectValue("lastName", "name.empty", "field can't be null");
//        }
//        if (checkInputString(String.valueOf(p.getAge()))) {
//            errors.rejectValue("age", "age.empty", "field can't be null");
//        }
//    }
//
//    private boolean checkInputString(String input) {
//        return (input == null || input.trim().length() == 0);
//    }
//}
